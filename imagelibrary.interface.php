<?php
interface Imagelibrary {

   public function load($filename); 
   public function save();
  
   public function getWidth();
   public function getHeight();
   public function resizeToHeight($height);
   public function resizeToWidth($width);
   public function scaleImage($scale);
   public function resizeImage($width,$height);
   public function textOnImage($text,$color,$size);
   public function rotateImage ($angle ,$bg_color);
 
}
?>

