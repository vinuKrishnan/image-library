<?php
require 'imagelibrary.interface.php';
class imglib implements Imagelibrary {
var $image;
var $image_type;

function loadfile($filename){
      $image_info=  getimagesize($filename);
      $this->image_type=$image_info[2];
     if($this->image_type== IMAGETYPE_PNG){
     echo "Image Type:PNG";
     $this->image=  imagecreatefrompng($filename);
    }
    if($this->image_type== IMAGETYPE_JPEG){
  echo "Image Type:Jpg";
  $this->image=  imagecreatefromjpeg($filename);
 }
   if($this->image_type== IMAGETYPE_GIF){
   echo "Image Type:GIF";
   $this->image=  imagecreatefromgif($filename);
  }
}



function getWidth() {
  return imagesx($this->image);


   }

function getHeight(){
  return imagesy($this->image);

  }

function rotateImage ($angle ,$bg_color) {
  if($this->image_type== IMAGETYPE_PNG){
  ob_end_clean();
  header('Content-type: image/png');
  $rotate = imagerotate($this->image, $angle, $bg_color);
  imagepng($rotate);
  imagedestroy($rotate);
  exit;
  }
   elseif($this->image_type== IMAGETYPE_JPEG){

  $rotate = imagerotate($this->image, $angle, $bg_color);
  return $rotate;
  exit;
      }
  elseif($this->image_type== IMAGETYPE_GIF){
  ob_end_clean();
  header('Content-type: image/gif');
  $rotate = imagerotate($this->image, $angle, $bg_color);
  imagegif($rotate);
  imagedestroy($rotate);
  exit;
        }
}

function save(){
  imagejpeg($this->image,"image/Desert_scaled.jpg");
  //imagedestroy($image);
}

function display(){
   ob_end_clean();
   header('Content-type: image/jpeg');
   imagejpeg($this->image);

}
function resizeImage($width,$height) {
$filename = 'Desert.jpg';
$thumb = imagecreatetruecolor($width, $height);
$source = imagecreatefromjpeg($filename);
$w=imagesx($this->image);
$h=imagesy($this->image);
imagecopyresized($thumb, $source, 0, 0, 0, 0, $width, $height, $w, $h);
imagejpeg($thumb,"image/Desert_resized.jpg");
imagedestroy($thumb);
}

function resizeToHeight($height) {

      $ratio = $height / $this->getHeight();

      $width = $this->getWidth() * $ratio;
      $this->resizeImage($width,$height);
      imagejpeg($this->image,"image/Desert_height_scaled.jpg");

   }


function resizeToWidth($width) {
    $ratio = $width / $this->getWidth();
      $height = $this->getHeight() * $ratio;
    $this->resizeImage($width,$height);
    imagejpeg($this->image,"image/Desert_width_scaled.jpg");

   }
   function scaleImage($scale) {
         $width = $this->getWidth() * $scale/100;
         $height = $this->getheight() * $scale/100;
         $this->resizeImage($width,$height);
         imagejpeg($this->image,"image/Desert_scaled.jpg");

      }


  function textOnImage()
  {
  $white = imagecolorallocate($this->image, 255, 255, 255);
  $font_path ='arial.ttf';
  $text = "This is a desert!";
  imagettftext($this->image, 25, 0, 75, 300, $white, $font_path, $text);
  imagejpeg($this->image,"image/Desert_Text.jpg");

  }

}



?>
